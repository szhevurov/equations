﻿using System.Collections.Generic;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.Extensions
{
    internal static class TokenListExtensions
    {
        internal static Token PreceedingTo(this IList<Token> tokens, int index)
        {
            return GetTokenByIndex(tokens, --index);
        }

        internal static Token NextTo(this IList<Token> tokens, int index)
        {
            return GetTokenByIndex(tokens, ++index);
        }

        private static Token GetTokenByIndex(IList<Token> tokens, int index)
        {
            if (index >= 0 && index < tokens.Count)
            {
                return tokens[index];
            }

            return null;
        }
    }
}