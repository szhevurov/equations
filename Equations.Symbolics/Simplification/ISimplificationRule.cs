﻿using System.Collections.Generic;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.Simplification
{
    internal interface ISimplificationRule
    {
        IList<Token> Simplify(IList<Token> tokens);
    }
}