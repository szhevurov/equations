﻿using System.Collections.Generic;
using System.Linq;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.Simplification
{
    internal sealed class ExpandParenthesesRule: ISimplificationRule
    {
        public IList<Token> Simplify(IList<Token> tokens)
        {
            var simplified = new List<Token>(tokens.Count);

            Token lastVisitedUnaryOperator = null;
            var preceedingUnaryOperators = new Stack<Token>();

            foreach (var token in tokens)
            {
                if (token.Type != TokenType.Operator)
                {
                    simplified.Add(token);
                    continue;
                }

                if (token.Value == Operators.OpenParenthesis)
                {
                    if (lastVisitedUnaryOperator != null)
                    {
                        preceedingUnaryOperators.Push(lastVisitedUnaryOperator);
                    }
                }
                else if (token.Value == Operators.CloseParenthesis)
                {
                    if (preceedingUnaryOperators.Any())
                    {
                        preceedingUnaryOperators.Pop();
                    }                    
                }
                else
                {
                    if (preceedingUnaryOperators.Count > 0 &&
                        preceedingUnaryOperators.Peek().Value == Operators.Subtract)
                    {
                        var inverted = this.CreateInvertedUnaryOperator(token);
                        simplified.Add(inverted);

                        lastVisitedUnaryOperator = inverted;

                        continue;
                    }

                    simplified.Add(token);
                    lastVisitedUnaryOperator = token;
                }
            }

            return simplified;
        }

        private Token CreateInvertedUnaryOperator(Token token)
        {
            var value = token.Value == Operators.Add ? Operators.Subtract : Operators.Add;
            return new Token(value, TokenType.Operator);
        }
    }
}