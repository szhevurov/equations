﻿using System.Collections.Generic;
using Equations.Symbolics.Extensions;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.SyntaxValidation
{
    internal sealed class ScalarTokenValidator: ITokenPositionValidator
    {
        public TokenType TokenType
        {
            get
            {
                return TokenType.Scalar;
            }
        }

        public void Validate(Token token, int index, IList<Token> tokens)
        {
            var preceedingToken = tokens.PreceedingTo(index);
            var nextToken = tokens.NextTo(index);

            if (preceedingToken != null)
            {
                if (preceedingToken.Type == TokenType.Variable || preceedingToken.Type == TokenType.Scalar)
                {
                    throw new SyntaxException();
                }
            }
            
            if (nextToken == null|| nextToken.Type == TokenType.Scalar || nextToken.Type == TokenType.Operator)
            {
                throw new SyntaxException();
            }
        }
    }
}