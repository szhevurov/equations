﻿using System.Collections.Generic;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.SyntaxValidation
{
    internal sealed class ParenthesisValidator : ISyntaxValidator
    {
        public void Validate(IList<Token> tokens)
        {
            var openBracketsCount = 0;
            foreach (var token in tokens)
            {
                if (token.Type != TokenType.Operator)
                {
                    continue;
                }

                switch (token.Value)
                {
                    case Operators.OpenParenthesis:
                        openBracketsCount++;
                        break;

                    case Operators.CloseParenthesis:
                        openBracketsCount--;
                        break;
                }
            }

            if (openBracketsCount != 0)
            {
                throw new SyntaxException("Opening and closing brackets does not match");
            }
        }
    }
}