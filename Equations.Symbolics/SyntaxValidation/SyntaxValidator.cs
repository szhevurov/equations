﻿using System.Collections.Generic;
using System.Linq;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.SyntaxValidation
{
    internal sealed class SyntaxValidator: ISyntaxValidator
    {
        private readonly List<ITokenPositionValidator> syntaxValidators;

        public SyntaxValidator()
        {
            this.syntaxValidators = new List<ITokenPositionValidator>
            {
                new ScalarTokenValidator(),
                new VariableTokenValidator(),
                new OperatorTokenValidator()
            };
        }

        public void Validate(IList<Token> tokens)
        {
            for (int index = 0; index < tokens.Count; index++)
            {
                var token = tokens[index];
                var validator = this.syntaxValidators.FirstOrDefault(v => v.TokenType == token.Type);

                if (validator == null)
                {
                    throw new SyntaxException("Unknown token type");
                }

                validator.Validate(token, index, tokens);
            }
        }
    }
}