﻿using System.Collections.Generic;
using Equations.Symbolics.Extensions;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.SyntaxValidation
{
    internal sealed class OperatorTokenValidator: ITokenPositionValidator
    {
        public TokenType TokenType
        {
            get
            {
                return TokenType.Operator;
            }
        }

        public void Validate(Token token, int index, IList<Token> tokens)
        {
            var preceedingToken = tokens.PreceedingTo(index);
            var nextToken = tokens.NextTo(index);

            
            this.ValidateOperator(token, preceedingToken, nextToken);
        }

        private void ValidateOperator(Token token, Token preceedingtoken, Token nextToken)
        {
            switch (token.Value)
            {
                case Operators.Add:
                case Operators.Subtract:
                    this.ValidateUnaryOperator(preceedingtoken, nextToken);
                    break;
                case Operators.OpenParenthesis:
                    this.ValidateOpenParenthesis(preceedingtoken, nextToken);
                    break;
                case Operators.CloseParenthesis:
                    this.ValidateCloseParenthesis(preceedingtoken, nextToken);
                    break;
            }
        }

        private void ValidateUnaryOperator(Token preceedingToken, Token nextToken)
        {
            if (nextToken == null)
            {
                throw new SyntaxException();
            }

            if (preceedingToken != null && preceedingToken.Type != TokenType.Variable && preceedingToken.Value != Operators.CloseParenthesis)
            {
                throw new SyntaxException();
            }

            if (nextToken == null || nextToken.Type == TokenType.Operator && nextToken.Value == Operators.CloseParenthesis)
            {
                throw new SyntaxException();
            }
        }

        private void ValidateOpenParenthesis(Token preceedingToken, Token nextToken)
        {
            if (nextToken == null)
            {
                throw  new SyntaxException();
            }

            if (preceedingToken != null && preceedingToken.Type != TokenType.Operator)
            {
                throw new SyntaxException();
            }

            if (preceedingToken != null && preceedingToken.Value == Operators.CloseParenthesis)
            {
                throw new SyntaxException();
            }

            if (nextToken.Type != TokenType.Scalar && nextToken.Type != TokenType.Variable && nextToken.Value != Operators.OpenParenthesis)
            {
                throw new SyntaxException();
            }
        }

        private void ValidateCloseParenthesis(Token preceedingToken, Token nextToken)
        {
            if (preceedingToken == null || (preceedingToken.Type != TokenType.Variable && preceedingToken.Value != Operators.CloseParenthesis))
            {
                throw new SyntaxException();
            }

            if (nextToken == null)
            {
                return;
            }

            if (nextToken.Type != TokenType.Operator)
            {
                throw new SyntaxException();
            }

            if (nextToken.Value == Operators.OpenParenthesis)
            {
                throw new SyntaxException();
            }
        }
    }
}