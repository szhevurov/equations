﻿using System.Collections.Generic;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.SyntaxValidation
{
    internal interface ITokenPositionValidator
    {
        TokenType TokenType { get; }
        void Validate(Token token, int index, IList<Token> tokens);
    }
}