﻿using System.Collections.Generic;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.SyntaxValidation
{
    internal interface ISyntaxValidator
    {
        void Validate(IList<Token> tokens);
    }
}