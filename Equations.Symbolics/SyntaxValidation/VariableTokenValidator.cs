﻿using System.Collections.Generic;
using Equations.Symbolics.Extensions;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.SyntaxValidation
{
    internal sealed class VariableTokenValidator: ITokenPositionValidator
    {
        public TokenType TokenType
        {
            get
            {
                return TokenType.Variable;
            }
        }

        public void Validate(Token token, int index, IList<Token> tokens)
        {
            var nextToken = tokens.NextTo(index);

            if (nextToken == null)
            {
                return;
            }

            if (nextToken.Type == TokenType.Scalar)
            {
                throw new SyntaxException();
            }
        }
    }
}