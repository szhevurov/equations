﻿using System;

namespace Equations.Symbolics
{
    internal sealed class TokenCharacterBuffer
    {
        private readonly string characters;

        public const char Terminator = '\n';

        internal int Position { get; private set; }

        internal bool IsAtEnd
        {
            get { return this.Position >= this.characters.Length; }
        }

        internal char CurrentCharacter
        {
            get { return this.PeekNext(); }
        }

        internal char this[int position]
        {
            get
            {
                if (position < 0)
                {
                    throw new InvalidOperationException();
                }

                return this.characters[position];
            }
        }

        internal TokenCharacterBuffer(string characters)
        {
            if (string.IsNullOrWhiteSpace(characters))
            {
                throw new ArgumentException();
            }

            this.characters = characters;
        }

        internal void MoveNext(int step = 1)
        {
            if (step <= 0)
            {
                throw new InvalidOperationException();
            }

            if (this.IsAtEnd)
            {
                return;
            }

            
            this.Position = this.Position + step;
        }

        internal char PeekNext(int step = 0)
        {
            if (step < 0)
            {
                throw new InvalidOperationException();
            }

            var index = this.Position + step;

            if (index >= this.characters.Length)
            {
                return Terminator;
            }

            return this.characters[index];
        }
    }
}