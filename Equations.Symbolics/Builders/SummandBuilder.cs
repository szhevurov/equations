﻿using System.Collections.Generic;
using System.Linq;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.Builders
{
    internal sealed class SummandBuilder: ISummandBuilder
    {
        public IEnumerable<Summand> Build(IList<Token> tokens)
        {
            Token lastOperator = null;

            var summands = new List<Summand>();

            for (int i = 0; i < tokens.Count;)
            {
                var token = tokens[i];
                if (token.Type == TokenType.Operator)
                {
                    lastOperator = token;
                    i++;
                }

                var summand = this.BuildSummand(ref i, tokens, lastOperator);

                if (summand != null)
                {
                    summands.Add(summand);
                }
            }

            return summands;
        }

        private Summand BuildSummand(ref int index, IList<Token> tokens, Token preceedingOperator)
        {
            double value = Summand.DefaultValue;
            var variables = new List<string>();

            var inverseValue = preceedingOperator != null && preceedingOperator.Value == Operators.Subtract;

            while (index < tokens.Count)
            {
                var token = tokens[index];
                switch (token.Type)
                {
                    case TokenType.Scalar:
                        double.TryParse(token.Value, out value);
                        break;
                    case TokenType.Variable:
                        variables.Add(token.Value);
                        break;

                    case TokenType.Operator:
                        return this.BuildSummand(value, variables, inverseValue);
                }

                index++;
            }

            return this.BuildSummand(value, variables, inverseValue);
        }

        private Summand BuildSummand(double value, List<string> variables, bool inverseValue)
        {
            if (!variables.Any())
            {
                return null;
            }

            var summand = new Summand(value, variables);
            if (inverseValue)
            {
                summand.InverseValue();
            }

            return summand;
        }
    }
}