﻿using System.Collections.Generic;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.Builders
{
    internal interface ISummandBuilder
    {
        IEnumerable<Summand> Build(IList<Token> tokens);
    }
}