﻿using System;
using System.Collections.Generic;
using System.Linq;
using Equations.Symbolics.Simplification;
using Equations.Symbolics.SyntaxValidation;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.Builders
{
    internal sealed class EquationBuilder : IEquationBuilder
    {
        private const char EqualsChar = '=';

        private const int EquationPartsCount = 2;

        public Equation Build(string equation, ISummandBuilder summandBuilder, ITokenizer tokenizer, ISyntaxValidator[] validators, ISimplificationRule[] rules)
        {
            var equationParts = equation.Split(new[] {EqualsChar}, StringSplitOptions.RemoveEmptyEntries);

            if (equationParts.Length != EquationPartsCount)
            {
                throw new SyntaxException();
            }

            var left = this.BuildEquationPart(equationParts.First(), summandBuilder, tokenizer, validators, rules);
            var right = this.BuildEquationPart(equationParts.Last(), summandBuilder, tokenizer, validators, rules);

            return new Equation(left, right);
        }

        private IEnumerable<Summand> BuildEquationPart(string equationPart, ISummandBuilder summandBuilder, ITokenizer tokenizer, ISyntaxValidator[] validators, ISimplificationRule[] rules)
        {
            var tokens = tokenizer.Tokenize(equationPart).ToList();
            
            this.Validate(tokens, validators);

            var simplified = this.Simplify(tokens, rules).ToList();

            return summandBuilder.Build(simplified);
        }

        private void Validate(IList<Token> tokens, ISyntaxValidator[] validators)
        {
            foreach (var validator in validators)
            {
                validator.Validate(tokens);
            }
        }

        private IEnumerable<Token> Simplify(IList<Token> tokens, ISimplificationRule[] rules)
        {
            return rules.Aggregate(tokens, (current, rule) => rule.Simplify(current));
        }
    }
}