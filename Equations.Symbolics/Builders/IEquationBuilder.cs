﻿using Equations.Symbolics.Simplification;
using Equations.Symbolics.SyntaxValidation;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics.Builders
{
    internal interface IEquationBuilder
    {
        Equation Build(string equation, ISummandBuilder summandBuilder, ITokenizer tokenizer, ISyntaxValidator[] validators, ISimplificationRule[] rules);
    }
}