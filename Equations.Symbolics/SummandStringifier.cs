﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Equations.Symbolics
{
    internal sealed class SummandStringifier
    {
        private const string Plus = " + ";

        private const string Minus = " - ";

        private const string EquationFormat = "{0} = {1}";

        private const string EmptyEquation = "0";

        private const string ValueFormat = "{0:0.###########}";

        internal string Stringify(Equation equation)
        {
            if (equation == null)
            {
                return EmptyEquation;
            }

            return this.Stringify(equation.Left, equation.Right);
        }

        internal string Stringify(IList<Summand> left, IList<Summand> right)
        {
            var leftAsString = this.StringifyEquationPart(left);
            var rightAsString = this.StringifyEquationPart(right);

            return string.Format(EquationFormat, leftAsString, rightAsString);
        }

        private string StringifyEquationPart(IList<Summand> summands)
        {
            if (summands == null || !summands.Any())
            {
                return EmptyEquation;
            }

            var sb = new StringBuilder();
            var summandsToPrint = summands.Where(s => !s.IsZero).ToList();

            for (int i = 0; i < summandsToPrint.Count; i++)
            {
                var summand = summandsToPrint[i];

                this.TryAppendSign(summand, sb, i);
                this.TryAppendValue(summand, sb);
                this.AppendVariables(summand, sb);
            }

            var result = sb.ToString();

            if (string.IsNullOrWhiteSpace(result))
            {
                return EmptyEquation;
            }

            return result;
        }

        private void TryAppendSign(Summand summand, StringBuilder builder, int position)
        {
            var signToPrint = summand.Value > 0 ? Plus : Minus;
            if (position == 0)
            {
                signToPrint = signToPrint == Minus ? signToPrint.Trim() : string.Empty;
            }

            builder.Append(signToPrint);
        }

        private void TryAppendValue(Summand summand, StringBuilder builder)
        {
            var absoluteValue = Math.Abs(summand.Value);

            if (absoluteValue != Summand.DefaultValue)
            {
                var valueToPrint = string.Format(ValueFormat, absoluteValue);
                builder.Append(valueToPrint);
            }
        }

        private void AppendVariables(Summand summand, StringBuilder builder)
        {
            var variables = string.Join(string.Empty, summand.Variables);
            builder.Append(variables);
        }
    }
}