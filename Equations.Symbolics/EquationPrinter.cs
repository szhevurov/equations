﻿using Equations.Symbolics.Builders;
using Equations.Symbolics.Simplification;
using Equations.Symbolics.SyntaxValidation;
using Equations.Symbolics.Tokenize;

namespace Equations.Symbolics
{
    public static class EquationPrinter
    {
        private static readonly ISyntaxValidator[] Validators = {
            new SyntaxValidator(),
            new ParenthesisValidator()
        };

        private static readonly ISimplificationRule[] Rules = {
            new ExpandParenthesesRule()
        };

        public static string PrintCanonicalForm(string equationString)
        {
            ITokenizer tokenizer = new Tokenizer();
            ISummandBuilder summandBuilder = new SummandBuilder();
            IEquationBuilder equationBuilder = new EquationBuilder();

            var stringifier = new SummandStringifier();

            var equation = equationBuilder.Build(equationString, summandBuilder, tokenizer, Validators, Rules);

            equation.ToCanonicalForm();

            return stringifier.Stringify(equation);
        }
    }
}