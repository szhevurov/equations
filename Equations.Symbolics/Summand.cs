﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Equations.Symbolics
{
    internal sealed class Summand
    {
        private const double Zero = 0d;

        internal const double DefaultValue = 1.0d;

        internal double Value { get; set; }

        internal IReadOnlyList<string> Variables { get; private set; }

        internal bool IsZero
        {
            get { return this.Value == Zero; }
        }

        internal Summand(IList<string> variables) : this(DefaultValue, variables)
        {
        }

        internal Summand(double value, IList<string> variables)
        {
            if (variables == null)
            {
                throw new ArgumentNullException("variables");
            }

            this.Value = value;
            this.Variables = new ReadOnlyCollection<string>(variables);
        }

        internal void InverseValue()
        {
            this.Value = -this.Value;
        }
    }
}