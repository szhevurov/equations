﻿using System.Collections.Generic;

namespace Equations.Symbolics.Tokenize
{
    internal interface ITokenizer
    {
        IEnumerable<Token> Tokenize(string input);
    }
}