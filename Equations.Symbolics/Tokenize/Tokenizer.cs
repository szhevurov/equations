﻿using System;
using System.Collections.Generic;
using Equations.Symbolics.Tokenize.Extractors;

namespace Equations.Symbolics.Tokenize
{
    internal sealed class Tokenizer : ITokenizer
    {
        private readonly List<ITokenExtractor> extractors;

        public Tokenizer()
        {
            this.extractors = new List<ITokenExtractor>
            {
                new NumberExtractor(),
                new VariableExtractor(),
                new OperatorExtractor()
            };
        }

        public IEnumerable<Token> Tokenize(string input)
        {
            if (string.IsNullOrWhiteSpace(input))
            {
                throw new ArgumentException();
            }

            var buffer = new TokenCharacterBuffer(input);
            var tokens = new List<Token>();

            while (!buffer.IsAtEnd)
            {
                this.SkipWhitespace(buffer);

                if (buffer.IsAtEnd)
                {
                    break;
                }

                var matchedExtractorFound = false;
                foreach (var extractor in this.extractors)
                {
                    if (extractor.Matches(buffer))
                    {
                        matchedExtractorFound = true;

                        var token = extractor.Extract(buffer);
                        tokens.Add(token);
                    }
                }

                if (!matchedExtractorFound)
                {
                    throw new SyntaxException();
                }
            }
            
            return tokens;
        }

        private void SkipWhitespace(TokenCharacterBuffer buffer)
        {
            while (!buffer.IsAtEnd && char.IsWhiteSpace(buffer.CurrentCharacter))
            {
                buffer.MoveNext();
            }
        }
    }
}