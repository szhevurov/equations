﻿namespace Equations.Symbolics.Tokenize
{
    internal sealed class Token
    {
        public string Value { get; private set; }

        public TokenType Type { get; private set; }
        
        public Token(string value, TokenType type)
        {
            Value = value;
            Type = type;
        }

        private bool Equals(Token other)
        {
            return string.Equals(this.Value, other.Value) && this.Type == other.Type;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;

            return obj is Token && Equals((Token) obj);
        }

        public override int GetHashCode()
        {
            const int prime = 397;
            unchecked
            {
                return ((this.Value != null ? this.Value.GetHashCode() : 0) * prime) ^ (int)this.Type;
            }
        }
    }
}
