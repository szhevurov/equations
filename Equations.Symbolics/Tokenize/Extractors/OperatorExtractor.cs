﻿namespace Equations.Symbolics.Tokenize.Extractors
{
    internal class OperatorExtractor: ITokenExtractor
    {
        public bool Matches(TokenCharacterBuffer buffer)
        {
            var character = buffer.CurrentCharacter.ToString();
            return character == Operators.Add 
                || character == Operators.Subtract 
                || character == Operators.OpenParenthesis 
                || character == Operators.CloseParenthesis;
        }

        public Token Extract(TokenCharacterBuffer buffer)
        {
            var value = buffer.CurrentCharacter.ToString();
            buffer.MoveNext();

            return new Token(value, TokenType.Operator);
        }
    }
}