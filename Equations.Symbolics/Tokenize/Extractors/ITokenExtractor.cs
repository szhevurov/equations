﻿namespace Equations.Symbolics.Tokenize.Extractors
{
    internal interface ITokenExtractor
    {
        bool Matches(TokenCharacterBuffer buffer);

        Token Extract(TokenCharacterBuffer buffer);
    }
}