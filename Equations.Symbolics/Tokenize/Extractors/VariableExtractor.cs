﻿using System.Text;

namespace Equations.Symbolics.Tokenize.Extractors
{
    internal class VariableExtractor: ITokenExtractor
    {
        private const char Pow = '^';

        public bool Matches(TokenCharacterBuffer buffer)
        {
            return char.IsLetter(buffer.CurrentCharacter);
        }

        public Token Extract(TokenCharacterBuffer buffer)
        {
            var sb = new StringBuilder();
            sb.Append(char.ToLower(buffer.CurrentCharacter));
            
            buffer.MoveNext();

            if (buffer.CurrentCharacter == Pow)
            {
                sb.Append(buffer.CurrentCharacter);
                buffer.MoveNext();

                if (!char.IsDigit(buffer.CurrentCharacter))
                {
                    throw new SyntaxException();
                }

                while (char.IsDigit(buffer.CurrentCharacter))
                {
                    sb.Append(buffer.CurrentCharacter);
                    buffer.MoveNext();
                }
            }

            return new Token(sb.ToString(), TokenType.Variable);
        }
    }
}