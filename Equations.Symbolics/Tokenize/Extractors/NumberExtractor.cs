﻿using System.Text;

namespace Equations.Symbolics.Tokenize.Extractors
{
    internal class NumberExtractor: ITokenExtractor
    {
        private const char Dot = '.';

        public bool Matches(TokenCharacterBuffer buffer)
        {
            return char.IsDigit(buffer.CurrentCharacter);
        }

        public Token Extract(TokenCharacterBuffer buffer)
        {
            var sb = new StringBuilder();
            int dotsCount = 0;
            int initialPosition = buffer.Position;

            while (!buffer.IsAtEnd && char.IsDigit(buffer.CurrentCharacter) || buffer.CurrentCharacter == Dot)
            {
                if (buffer.CurrentCharacter == Dot && ++dotsCount > 1)
                {
                    throw new SyntaxException();
                }

                sb.Append(buffer.CurrentCharacter);
                buffer.MoveNext();
            }

            return new Token(sb.ToString(), TokenType.Scalar);
        }
    }
}