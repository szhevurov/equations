﻿namespace Equations.Symbolics.Tokenize
{
    internal enum TokenType
    {
        Scalar,
        Variable,
        Operator
    }
}