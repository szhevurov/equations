﻿using System.Collections.Generic;
using System.Linq;

namespace Equations.Symbolics
{
    internal sealed class Equation
    {
        internal IList<Summand> Left { get; private set; }

        internal IList<Summand> Right { get; private set; }

        internal bool IsEmpty
        {
            get { return !this.Left.Any() && this.Right.Any(); }
        }

        internal Equation(IEnumerable<Summand> left, IEnumerable<Summand> right)
        {
            this.Left = left != null ? left.ToList() : new List<Summand>();
            this.Right = right != null ? right.ToList() : new List<Summand>();
        }

        internal void ToCanonicalForm()
        {
            this.MergeParts();
            this.Left = this.ReduceSummands(this.Left);
        }

        private void MergeParts()
        {
            if (this.Right == null)
            {
                return;
            }

            foreach (var summand in this.Right)
            {
                summand.InverseValue();
                this.Left.Add(summand);
            }

            this.Right = null;
        }

        private IList<Summand> ReduceSummands(IEnumerable<Summand> summands)
        {
            var map = new Dictionary<int, Summand>();
            foreach (var s in summands)
            {
                var index = this.ComputeSummandSimilarityIndex(s);
                if (map.ContainsKey(index))
                {
                    map[index].Value += s.Value;
                    continue;
                }

                map.Add(index, s);
            }

            return map.Values.ToList();
        }

        private int ComputeSummandSimilarityIndex(Summand summand)
        {
            var hash = 0;
            foreach (var variable in summand.Variables)
            {
                unchecked
                {
                    hash = hash + variable.GetHashCode();
                }
            }

            return hash;
        }
    }
}