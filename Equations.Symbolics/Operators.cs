﻿namespace Equations.Symbolics
{
    internal static class Operators
    {
        internal const string Add = "+";

        internal const string Subtract = "-";

        internal const string OpenParenthesis = "(";

        internal const string CloseParenthesis = ")";
    }
}