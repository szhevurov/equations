﻿using Equation.Symbolics.Test.Helpers;
using Equations.Symbolics;
using Equations.Symbolics.Tokenize.Extractors;
using NUnit.Framework;

namespace Equation.Symbolics.Test.TokenizationTests
{
    [TestFixture]
    public class NumberExtractorTests
    {
        private readonly ITokenExtractor extractor = new NumberExtractor();

        [TestCase("2.22", ExpectedResult = Constants.FractionalTwo)]
        [TestCase("2.22x", ExpectedResult = Constants.FractionalTwo)]
        public string Test_Extraction_Of_Float(string input)
        {
            var result = this.extractor.Extract(new TokenCharacterBuffer(input));
            return result.Value;
        }

        [TestCase("2", ExpectedResult = Constants.Two)]
        [TestCase("2x", ExpectedResult = Constants.Two)]
        public string Test_Extraction_Of_Integer(string input)
        {
            var result = this.extractor.Extract(new TokenCharacterBuffer(input));
            return result.Value;
        }
    }
}