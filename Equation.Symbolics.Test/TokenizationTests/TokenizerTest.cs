﻿using System.Collections.Generic;
using Equation.Symbolics.Test.Helpers;
using Equations.Symbolics;
using Equations.Symbolics.Tokenize;
using NUnit.Framework;

namespace Equation.Symbolics.Test.TokenizationTests
{
    [TestFixture]
    public class TokenizerTest
    {
        private const string X = "x";

        private const string Scalarvalue = "2.2";

        private const string Y = "y^2";

        private readonly ITokenizer tokenizer = new Tokenizer();

        private readonly List<Token> singleSummand = new List<Token>
        {
            Tokens.Scalar(Scalarvalue), Tokens.Variable(X), Tokens.Variable(Y)
        };

        private readonly List<Token> multipleSummands = new List<Token>
        {
            Tokens.Scalar(Scalarvalue), Tokens.Variable(X), Tokens.Variable(Y),
            Tokens.Operator(Operators.Add), Tokens.Variable(Y), Tokens.Variable(X)
        };

        private readonly List<Token> multipleSummandsWithParenthesis = new List<Token>
        {
            Tokens.Scalar(Scalarvalue), Tokens.Variable(X), Tokens.Variable(Y),
            Tokens.Operator(Operators.Add), Tokens.Variable(Y), Tokens.Variable(X),
            Tokens.Operator(Operators.Subtract),Tokens.Operator(Operators.OpenParenthesis),
            Tokens.Variable(X), Tokens.Operator(Operators.Add), Tokens.Variable(Y),
            Tokens.Operator(Operators.CloseParenthesis)
        };

        [TestCase("2.2xy^2")]
        [TestCase(" 2.2xy^2 ")]
        [TestCase(" 2.2 x y^2 ")]
        public void Test_Conversion_Of_Single_Summand(string input)
        {
            var result = this.tokenizer.Tokenize(input);
            CollectionAssert.AreEqual(this.singleSummand, result);
        }

        [TestCase("2.2xy^2+y^2x")]
        [TestCase(" 2.2xy^2 + y^2x ")]
        [TestCase(" 2.2 x y^2 + y^2 x ")]
        public void Test_Conversion_Of_Multiple_Summands(string input)
        {
            var result = this.tokenizer.Tokenize(input);
            CollectionAssert.AreEqual(this.multipleSummands, result);
        }

        [TestCase("2.2xy^2+y^2x-(x+y^2)")]
        [TestCase(" 2.2xy^2 + y^2x - ( x + y^2 ) ")]
        [TestCase(" 2.2 x y^2 + y^2 x - ( x + y^2 ) ")]
        public void Test_Conversion_Of_Multiple_Summands_With_Parenthesis(string input)
        {
            var result = this.tokenizer.Tokenize(input);
            CollectionAssert.AreEqual(this.multipleSummandsWithParenthesis, result);
        }
    }
}