﻿using Equation.Symbolics.Test.Helpers;
using Equations.Symbolics;
using Equations.Symbolics.Tokenize.Extractors;
using NUnit.Framework;

namespace Equation.Symbolics.Test.TokenizationTests
{
    [TestFixture]
    public class VariableExtractorTests
    {
        private readonly ITokenExtractor extractor = new VariableExtractor();

        [TestCase("x^2", ExpectedResult = Constants.X2)]        
        [TestCase("x^2y", ExpectedResult = Constants.X2)]                
        [TestCase("x^2.3", ExpectedResult = Constants.X2)]                
        public string Test_Extraction_Of_Variable_Raised_To_The_Power(string input)
        {
            var result = this.extractor.Extract(new TokenCharacterBuffer(input));
            return result.Value;
        }

        [TestCase("x", ExpectedResult = Constants.X)]
        [TestCase("xy", ExpectedResult = Constants.X)]        
        public string Test_Extraction_Of_Variable(string input)
        {
            var result = this.extractor.Extract(new TokenCharacterBuffer(input));
            return result.Value;
        }
    }
}