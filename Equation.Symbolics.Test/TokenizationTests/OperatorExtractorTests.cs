﻿using Equations.Symbolics;
using Equations.Symbolics.Tokenize.Extractors;
using NUnit.Framework;

namespace Equation.Symbolics.Test.TokenizationTests
{
    [TestFixture]
    public class OperatorExtractorTests
    {
        private readonly ITokenExtractor extractor = new OperatorExtractor();

        [TestCase("+", ExpectedResult = Operators.Add)]
        [TestCase("+12", ExpectedResult = Operators.Add)]
        [TestCase("+x", ExpectedResult = Operators.Add)]
        [TestCase("+(", ExpectedResult = Operators.Add)]
        public string Test_Extraction_Of_Plus_Operator(string input)
        {
            var result = this.extractor.Extract(new TokenCharacterBuffer(input));
            return result.Value;
        }

        [TestCase("-", ExpectedResult = Operators.Subtract)]
        [TestCase("-12", ExpectedResult = Operators.Subtract)]
        [TestCase("-x", ExpectedResult = Operators.Subtract)]
        [TestCase("-(", ExpectedResult = Operators.Subtract)]
        public string Test_Extraction_Of_Minus_Operator(string input)
        {
            var result = this.extractor.Extract(new TokenCharacterBuffer(input));
            return result.Value;
        }

        [TestCase("(", ExpectedResult = Operators.OpenParenthesis)]
        [TestCase("(x", ExpectedResult = Operators.OpenParenthesis)]
        [TestCase("()", ExpectedResult = Operators.OpenParenthesis)]
        public string Test_Extraction_Of_Open_Parenthesis_Operator(string input)
        {
            var result = this.extractor.Extract(new TokenCharacterBuffer(input));
            return result.Value;
        }

        [TestCase(")", ExpectedResult = Operators.CloseParenthesis)]
        [TestCase(")+", ExpectedResult = Operators.CloseParenthesis)]        
        public string Test_Extraction_Of_Close_Parenthesis_Operator(string input)
        {
            var result = this.extractor.Extract(new TokenCharacterBuffer(input));
            return result.Value;
        }
    }
}