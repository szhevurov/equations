﻿using System.Collections.Generic;
using Equation.Symbolics.Test.Helpers;
using Equations.Symbolics;
using Equations.Symbolics.SyntaxValidation;
using Equations.Symbolics.Tokenize;
using NUnit.Framework;

namespace Equation.Symbolics.Test.SyntaxValidation
{
    [TestFixture]
    public class OperatorTokenValidatorTest
    {
        private readonly ITokenPositionValidator validator = new OperatorTokenValidator();

        #region Unary operators tests

        [Test]
        public void Test_Validation_Of_Single_Unary()
        {
            var addOperator = Tokens.Operator(Operators.Add);
            var tokens = new List<Token> { addOperator };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(addOperator, 0, tokens));
        }

        [Test]
        public void Test_Validation_Of_Unary_And_Preceeding_Scalar()
        {
            var addOperator = Tokens.Operator(Operators.Add);
            var tokens = new List<Token> { Tokens.Scalar(Constants.FractionalTwo), addOperator };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(addOperator, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Unary_And_Preceeding_Open_Parenthesis_And_Subsequent_Variable()
        {
            var addOperator = Tokens.Operator(Operators.Add);
            var tokens = new List<Token> { Tokens.Operator(Operators.OpenParenthesis), addOperator, Tokens.Variable(Constants.X) };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(addOperator, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Unary_And_Subsequent_Close_Parenthesis()
        {
            var addOperator = Tokens.Operator(Operators.Add);
            var tokens = new List<Token> { addOperator, Tokens.Operator(Operators.CloseParenthesis) };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(addOperator, 0, tokens));
        }

        [Test]
        public void Test_Validation_Of_Unary_And_Preceeding_Close_Parenthesis_And_Subsequent_Variable()
        {
            var addOperator = Tokens.Operator(Operators.Add);
            var tokens = new List<Token> { Tokens.Operator(Operators.CloseParenthesis), addOperator, Tokens.Variable(Constants.X) };

            Assert.DoesNotThrow(() => this.validator.Validate(addOperator, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Unary_And_Subsequent_Open_Parenthesis()
        {
            var addOperator = Tokens.Operator(Operators.Add);
            var tokens = new List<Token> { addOperator, Tokens.Operator(Operators.OpenParenthesis)  };

            Assert.DoesNotThrow(() => this.validator.Validate(addOperator, 0, tokens));
        }

        #endregion

        #region Open parenthesis tests

        [Test]
        public void Test_Validation_Of_Single_Open_Parenthesis()
        {
            var parenthesis = Tokens.Operator(Operators.OpenParenthesis);
            var tokens = new List<Token> { parenthesis };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(parenthesis, 0, tokens));
        }
        
        [Test]
        public void Test_Validation_Of_Open_Parenthesis_And_Preceeding_Unary_Operator()
        {
            var parenthesis = Tokens.Operator(Operators.OpenParenthesis);
            var tokens = new List<Token> {Tokens.Operator(Operators.Add), parenthesis, Tokens.Variable(Constants.X) };

            Assert.DoesNotThrow(() => this.validator.Validate(parenthesis, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Open_Parenthesis_And_Preceeding_Another_Open_Parenthesis()
        {
            var parenthesis = Tokens.Operator(Operators.OpenParenthesis);
            var tokens = new List<Token> { Tokens.Operator(Operators.OpenParenthesis), parenthesis, Tokens.Variable(Constants.X) };

            Assert.DoesNotThrow(() => this.validator.Validate(parenthesis, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Open_Parenthesis_And_Subsequent_Another_Open_Parenthesis()
        {
            var parenthesis = Tokens.Operator(Operators.OpenParenthesis);
            var tokens = new List<Token> { parenthesis, Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X) };

            Assert.DoesNotThrow(() => this.validator.Validate(parenthesis, 0, tokens));
        }

        [Test]
        public void Test_Validation_Of_Open_Parenthesis_And_Preceeding_Closing_Parenthesis()
        {
            var parenthesis = Tokens.Operator(Operators.OpenParenthesis);
            var tokens = new List<Token> { Tokens.Operator(Operators.CloseParenthesis), parenthesis, Tokens.Scalar(Constants.Two) };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(parenthesis, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Open_Parenthesis_And_Subsequent_Closing_Parenthesis()
        {
            var parenthesis = Tokens.Operator(Operators.OpenParenthesis);
            var tokens = new List<Token> { parenthesis, Tokens.Operator(Operators.CloseParenthesis) };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(parenthesis, 0, tokens));
        }

        #endregion

        #region Close parenthesis tests

        [Test]
        public void Test_Validation_Of_Single_Close_Parenthesis()
        {
            var parenthesis = Tokens.Operator(Operators.CloseParenthesis);
            var tokens = new List<Token> { parenthesis };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(parenthesis, 0, tokens));
        }

        [Test]
        public void Test_Validation_Of_Close_Parenthesis_And_Preceeding_Unary_Operator()
        {
            var parenthesis = Tokens.Operator(Operators.CloseParenthesis);
            var tokens = new List<Token> { Tokens.Operator(Operators.Add), parenthesis};

            Assert.Throws<SyntaxException>(() => this.validator.Validate(parenthesis, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Close_Parenthesis_And_Preceeding_And_Subsequent_Another_Close_Parenthesis()
        {
            var parenthesis = Tokens.Operator(Operators.CloseParenthesis);
            var tokens = new List<Token> { Tokens.Operator(Operators.CloseParenthesis), parenthesis, Tokens.Operator(Operators.CloseParenthesis) };

            Assert.DoesNotThrow(() => this.validator.Validate(parenthesis, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Close_Parenthesis_And_Preceeding_Open_Parenthesis()
        {
            var parenthesis = Tokens.Operator(Operators.OpenParenthesis);
            var tokens = new List<Token> { Tokens.Operator(Operators.OpenParenthesis), parenthesis };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(parenthesis, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Close_Parenthesis_And_Subsequent_Variable()
        {
            var parenthesis = Tokens.Operator(Operators.CloseParenthesis);
            var tokens = new List<Token> { Tokens.Variable(Constants.X), parenthesis, Tokens.Variable(Constants.X) };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(parenthesis, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Close_Parenthesis_And_Preceeding_Variable()
        {
            var parenthesis = Tokens.Operator(Operators.CloseParenthesis);
            var tokens = new List<Token> { Tokens.Variable(Constants.X), parenthesis };

            Assert.DoesNotThrow(() => this.validator.Validate(parenthesis, 1, tokens));
        }

        #endregion
    }
}