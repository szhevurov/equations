﻿using System.Collections.Generic;
using Equation.Symbolics.Test.Helpers;
using Equations.Symbolics;
using Equations.Symbolics.SyntaxValidation;
using Equations.Symbolics.Tokenize;
using NUnit.Framework;

namespace Equation.Symbolics.Test.SyntaxValidation
{
    [TestFixture]
    public class ParenthesisValidatorTests
    {
        private readonly ISyntaxValidator validator = new ParenthesisValidator();

        private readonly IList<Token> noParentheses = new List<Token>
        {
            Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add), Tokens.Variable(Constants.Y)    
        };

        private readonly IList<Token> validParentheses = new List<Token>
        {
            Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add), Tokens.Variable(Constants.Y), Tokens.Operator(Operators.CloseParenthesis)
        };

        private readonly IList<Token> invalidNumberOfOpeningParentheses = new List<Token>
        {
            Tokens.Operator(Operators.OpenParenthesis), Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X),
            Tokens.Operator(Operators.Add), Tokens.Variable(Constants.Y), Tokens.Operator(Operators.CloseParenthesis)
        };

        private readonly IList<Token> invalidNumberOfClosingParentheses = new List<Token>
        {
            Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),
            Tokens.Variable(Constants.Y), Tokens.Operator(Operators.CloseParenthesis), Tokens.Operator(Operators.CloseParenthesis)
        };

        [Test]
        public void Test_Validation_Of_Tokens_Without_Parentheses()
        {
            Assert.DoesNotThrow(() => this.validator.Validate(this.noParentheses));
        }

        [Test]
        public void Test_Validation_Of_Valid_Parentheses()
        {
            Assert.DoesNotThrow(() => this.validator.Validate(this.validParentheses));
        }

        [Test]
        public void Test_Validation_Of_Tokens_With_invalid_Number_Of_Opening_Parentheses()
        {
            Assert.Throws<SyntaxException>(() => this.validator.Validate(this.invalidNumberOfOpeningParentheses));
        }

        [Test]
        public void Test_Validation_Of_Tokens_With_invalid_Number_Of_Closing_Parentheses()
        {
            Assert.Throws<SyntaxException>(() => this.validator.Validate(this.invalidNumberOfClosingParentheses));
        }
    }
}