﻿using System.Collections.Generic;
using Equation.Symbolics.Test.Helpers;
using Equations.Symbolics.SyntaxValidation;
using Equations.Symbolics.Tokenize;
using NUnit.Framework;

namespace Equation.Symbolics.Test.SyntaxValidation
{
    [TestFixture]
    public class VariableTokenValidatorTests
    {
        private readonly ITokenPositionValidator validator = new VariableTokenValidator();
        
        [Test]
        public void Test_Validation_Of_Single_Variable()
        {
            var variable = Tokens.Variable(Constants.X);
            var tokens = new List<Token> {variable};

            Assert.DoesNotThrow(() => this.validator.Validate(variable, 0, tokens));
        }

        [Test]
        public void Test_Validation_Of_Multiple_Adjacent_Variables()
        {
            var variable = Tokens.Variable(Constants.X);
            var tokens = new List<Token> { Tokens.Variable(Constants.Z), variable, Tokens.Variable(Constants.Y) };

            Assert.DoesNotThrow(() => this.validator.Validate(variable, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Adjacent_Variables_And_Scalars()
        {
            var variable = Tokens.Variable(Constants.X);
            var tokens = new List<Token> { Tokens.Scalar(Constants.Two), variable, Tokens.Variable(Constants.Y) };

            Assert.DoesNotThrow(() => this.validator.Validate(variable, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Variable_And_Scalar_Standing_After_Variable()
        {
            var variable = Tokens.Variable(Constants.X);
            var tokens = new List<Token> { variable, Tokens.Scalar(Constants.Two) };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(variable, 0, tokens));
        }
    }
}