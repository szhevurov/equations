﻿using System.Collections.Generic;
using Equation.Symbolics.Test.Helpers;
using Equations.Symbolics;
using Equations.Symbolics.SyntaxValidation;
using Equations.Symbolics.Tokenize;
using NUnit.Framework;

namespace Equation.Symbolics.Test.SyntaxValidation
{
    [TestFixture]
    public class ScalarTokenValidatorTests
    {
        private readonly ITokenPositionValidator validator = new ScalarTokenValidator();

        [Test]
        public void Test_Validation_Of_Single_Scalar()
        {
            var scalar = Tokens.Scalar(Constants.FractionalTwo);
            var tokens = new List<Token> {scalar};

            Assert.Throws<SyntaxException>(() => this.validator.Validate(scalar, 0, tokens));
        }

        [Test]
        public void Test_Validation_Of_Scalar_And_Subsequent_Operator()
        {
            var scalar = Tokens.Scalar(Constants.FractionalTwo);
            var tokens = new List<Token> { scalar, Tokens.Operator(Operators.Add) };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(scalar, 0, tokens));
        }

        [Test]
        public void Test_Validation_Of_Scalar_And_Preceeding_Variable()
        {
            var scalar = Tokens.Scalar(Constants.FractionalTwo);
            var tokens = new List<Token> { Tokens.Variable(Constants.X), scalar };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(scalar, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Two_Adjacent_Scalar_Tokens()
        {
            var scalar = Tokens.Scalar(Constants.FractionalTwo);
            var tokens = new List<Token> { Tokens.Scalar(Constants.FractionalTwo), scalar };

            Assert.Throws<SyntaxException>(() => this.validator.Validate(scalar, 1, tokens));
        }

        [Test]
        public void Test_Validation_Of_Operator_Scalar_Variable_Sequence()
        {
            var scalar = Tokens.Scalar(Constants.FractionalTwo);
            var tokens = new List<Token> { Tokens.Operator(Operators.Add), scalar, Tokens.Variable(Constants.X) };

            Assert.DoesNotThrow(() => this.validator.Validate(scalar, 1, tokens));
        }
    }
}