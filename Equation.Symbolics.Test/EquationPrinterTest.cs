﻿using Equations.Symbolics;
using Equations.Symbolics.Tokenize;
using NUnit.Framework;

namespace Equation.Symbolics.Test
{
    [TestFixture]
    public class EquationPrinterTest
    {
        [TestCase("0x + 0y = 0x - 0y", ExpectedResult = "0 = 0")]
        [TestCase("2x - y = 2x - y", ExpectedResult = "0 = 0")]
        [TestCase("x + y = x - y", ExpectedResult = "2y = 0")]
        [TestCase("-x + y = x - y", ExpectedResult = "-2x + 2y = 0")]
        [TestCase("3x^2y^2 + x^2y^2 = x - y", ExpectedResult = "4x^2y^2 - x + y = 0")]
        [TestCase("3x^2y^2 + 2y^2x^2 = 3xy-(x - y)", ExpectedResult = "5x^2y^2 - 3xy + x - y = 0")]
        [TestCase("3x^2y^2 + 2y^2x^2z = 3xy-(x - y)", ExpectedResult = "3x^2y^2 + 2y^2x^2z - 3xy + x - y = 0")]
        [TestCase("((2x^2 + x) - 2y) - y = -(2x + y^2) + z^253", ExpectedResult = "2x^2 + 3x - 3y + y^2 - z^253 = 0")]
        [TestCase("((2.32x^2 + 1.0001x) - 2.5y) - y = -(2x + y^2) + 0.00001z^253", ExpectedResult = "2.32x^2 + 3.0001x - 3.5y + y^2 - 0.00001z^253 = 0")]
        public string Test_Reduction_To_Canonical_Form(string input)
        {
            return EquationPrinter.PrintCanonicalForm(input);
        }

        [Test]
        [TestCase("3x^2y^2 + 2y^2x^2 = 3xy-((x - y)")]
        [TestCase("3x^2y^2 + 2y^2x^2)) = 3xy-((x - y))")]
        [TestCase("3x^2y^2 + 2y^2x^2)) = ")]
        [TestCase("3x^2y^2 + 2y^2x^2 = 12")]
        [TestCase("3x^2y^2 + 2y^2x^2+ = 12x")]
        public void Test_Reduction_Of_Invalid_Equation(string input)
        {
            Assert.Throws<SyntaxException>(() => EquationPrinter.PrintCanonicalForm(input));
        }
    }
}