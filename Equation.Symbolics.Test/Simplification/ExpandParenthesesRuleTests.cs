﻿using System.Collections.Generic;
using Equation.Symbolics.Test.Helpers;
using Equations.Symbolics;
using Equations.Symbolics.Simplification;
using Equations.Symbolics.Tokenize;
using NUnit.Framework;

namespace Equation.Symbolics.Test.Simplification
{
    [TestFixture]
    public class ExpandParenthesesRuleTests
    {
        private readonly ISimplificationRule rule = new ExpandParenthesesRule();

        #region Test Data

        // (x + x^2)
        private readonly IList<Token> testCase1 = new List<Token>
        {
            Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),
            Tokens.Variable(Constants.X2), Tokens.Operator(Operators.CloseParenthesis)
        };

        // x + x^2
        private readonly IList<Token> expectedResult1 = new List<Token>
        {
            Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add), Tokens.Variable(Constants.X2)
        };

        // -(x + x^2)
        private readonly IList<Token> testCase2 = new List<Token>
        {
            Tokens.Operator(Operators.Subtract), Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),
            Tokens.Variable(Constants.X2), Tokens.Operator(Operators.CloseParenthesis), 
        };

        // -x - x^2
        private readonly IList<Token> expectedResult2 = new List<Token>
        {
            Tokens.Operator(Operators.Subtract), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Subtract), Tokens.Variable(Constants.X2)
        };

        // (x + x^2) - (x + x^2)
        private readonly IList<Token> testCase3 = new List<Token>
        {
            Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),
            Tokens.Variable(Constants.X2), Tokens.Operator(Operators.CloseParenthesis), Tokens.Operator(Operators.Subtract),
            Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),
            Tokens.Variable(Constants.X2), Tokens.Operator(Operators.CloseParenthesis)
        };

        // x + x^2 - x - x^2
        private readonly IList<Token> expectedResult3 = new List<Token>
        {
            Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add), Tokens.Variable(Constants.X2),
            Tokens.Operator(Operators.Subtract), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Subtract),
            Tokens.Variable(Constants.X2)
        };

        // -((x + x^2) - (x + x^2) + x)
        private readonly IList<Token> testCase4 = new List<Token>
        {
            Tokens.Operator(Operators.Subtract), Tokens.Operator(Operators.OpenParenthesis), Tokens.Operator(Operators.OpenParenthesis),
            Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),Tokens.Variable(Constants.X2), Tokens.Operator(Operators.CloseParenthesis),
            Tokens.Operator(Operators.Subtract), Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),
            Tokens.Variable(Constants.X2), Tokens.Operator(Operators.CloseParenthesis), Tokens.Operator(Operators.Add), Tokens.Variable(Constants.X), Tokens.Operator(Operators.CloseParenthesis)
        };

        // -x - x^2 + x + x^2 - x
        private readonly IList<Token> expectedResult4 = new List<Token>
        {
            Tokens.Operator(Operators.Subtract), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Subtract),Tokens.Variable(Constants.X2),
            Tokens.Operator(Operators.Add), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),Tokens.Variable(Constants.X2),
            Tokens.Operator(Operators.Subtract), Tokens.Variable(Constants.X)
        };

        // (((x + x^2) - (x + x^2) + x))
        private readonly IList<Token> testCase5 = new List<Token>
        {
            Tokens.Operator(Operators.OpenParenthesis), Tokens.Operator(Operators.OpenParenthesis), Tokens.Operator(Operators.OpenParenthesis),
            Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),Tokens.Variable(Constants.X2), Tokens.Operator(Operators.CloseParenthesis),
            Tokens.Operator(Operators.Subtract), Tokens.Operator(Operators.OpenParenthesis), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),
            Tokens.Variable(Constants.X2), Tokens.Operator(Operators.CloseParenthesis), Tokens.Operator(Operators.Add), Tokens.Variable(Constants.X),
            Tokens.Operator(Operators.CloseParenthesis), Tokens.Operator(Operators.OpenParenthesis)
        };

        // -x - x^2 + x + x^2 - x
        private readonly IList<Token> expectedResult5 = new List<Token>
        {
            Tokens.Variable(Constants.X), Tokens.Operator(Operators.Add),Tokens.Variable(Constants.X2),
            Tokens.Operator(Operators.Subtract), Tokens.Variable(Constants.X), Tokens.Operator(Operators.Subtract),Tokens.Variable(Constants.X2),
            Tokens.Operator(Operators.Add), Tokens.Variable(Constants.X)
        };

        #endregion

        [Test]
        public void Test_Parentheses_Expanding_On_First_Test_Case()
        {
            var result = this.rule.Simplify(this.testCase1);
            CollectionAssert.AreEqual(this.expectedResult1, result);
        }

        [Test]
        public void Test_Parentheses_Expanding_On_Second_Test_Case()
        {
            var result = this.rule.Simplify(this.testCase2);
            CollectionAssert.AreEqual(this.expectedResult2, result);
        }

        [Test]
        public void Test_Parentheses_Expanding_On_Third_Test_Case()
        {
            var result = this.rule.Simplify(this.testCase3);
            CollectionAssert.AreEqual(this.expectedResult3, result);
        }

        [Test]
        public void Test_Parentheses_Expanding_On_Fourth_Test_Case()
        {
            var result = this.rule.Simplify(this.testCase4);
            CollectionAssert.AreEqual(this.expectedResult4, result);
        }

        [Test]
        public void Test_Parentheses_Expanding_On_Fifth_Test_Case()
        {
            var result = this.rule.Simplify(this.testCase5);
            CollectionAssert.AreEqual(this.expectedResult5, result);
        }
    }
}