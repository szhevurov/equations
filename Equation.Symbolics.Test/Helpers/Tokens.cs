﻿using Equations.Symbolics.Tokenize;

namespace Equation.Symbolics.Test.Helpers
{
    internal static class Tokens
    {
        internal static Token Scalar(string value)
        {
            return new Token(value, TokenType.Scalar);
        }

        internal static Token Variable(string name)
        {
            return new Token(name, TokenType.Variable);
        }

        internal static Token Operator(string op)
        {
            return new Token(op, TokenType.Operator);
        }
    }
}