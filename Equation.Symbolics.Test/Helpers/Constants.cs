﻿namespace Equation.Symbolics.Test.Helpers
{
    internal static class Constants
    {
        internal const string X = "x";

        internal const string X2 = "x^2";

        internal const string Y = "y";

        internal const string Y2 = "y^2";

        internal const string Z = "z";

        internal const string Z2 = "z^2";

        internal const string Two = "2";

        internal const string FractionalTwo = "2.22";
    }
}