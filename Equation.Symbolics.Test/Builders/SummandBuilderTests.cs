﻿using System.Collections.Generic;
using System.Linq;
using Equation.Symbolics.Test.Helpers;
using Equations.Symbolics;
using Equations.Symbolics.Builders;
using Equations.Symbolics.Tokenize;
using NUnit.Framework;

namespace Equation.Symbolics.Test.Builders
{
    [TestFixture]
    public class SummandBuilderTests
    {
        private readonly ISummandBuilder builder = new SummandBuilder();

        [Test]
        public void Test_Building_Of_Single_Summand_Without_Variabless()
        {
            // -2.22
            var tokens = new List<Token>
            {
                Tokens.Operator(Operators.Subtract),
                Tokens.Scalar(Constants.FractionalTwo)
            };

            var summand = this.builder.Build(tokens).FirstOrDefault();

            Assert.That(summand, Is.Null);
        }

        [Test]
        public void Test_Building_Of_Single_Summand_From_Tokens_Starting_With_Subtract_Operator()
        {
            // -2.22xy^2
            var tokens = new List<Token>
            {
                Tokens.Operator(Operators.Subtract),
                Tokens.Scalar(Constants.FractionalTwo),
                Tokens.Variable(Constants.X),
                Tokens.Variable(Constants.Y2)
            };

            var summand = this.builder.Build(tokens).FirstOrDefault();
            
            Assert.That(summand, Is.Not.Null);
            Assert.That(summand.Value, Is.EqualTo(-2.22d));
            Assert.That(summand.Variables.Count, Is.EqualTo(2));
        }


        [Test]
        public void Test_Building_Of_Single_Summand_From_Tokens_Starting_With_Add_Operator()
        {
            // +2xy^2
            var tokens = new List<Token>
            {
                Tokens.Operator(Operators.Add),
                Tokens.Scalar(Constants.Two),
                Tokens.Variable(Constants.X),
                Tokens.Variable(Constants.Y2)
            };

            var summand = this.builder.Build(tokens).FirstOrDefault();

            Assert.That(summand, Is.Not.Null);
            Assert.That(summand.Value, Is.EqualTo(2d));
            Assert.That(summand.Variables.Count, Is.EqualTo(2));
        }

        [Test]
        public void Test_Building_Of_Single_Summand_From_Scalar_And_Two_Variables()
        {
            // 2xy^2
            var tokens = new List<Token>
            {
                Tokens.Scalar(Constants.Two),
                Tokens.Variable(Constants.X),
                Tokens.Variable(Constants.Y2)
            };

            var summand = this.builder.Build(tokens).FirstOrDefault();

            Assert.That(summand, Is.Not.Null);
            Assert.That(summand.Value, Is.EqualTo(2d));
            Assert.That(summand.Variables.Count, Is.EqualTo(2));
        }

        [Test]
        public void Test_Building_Of_Single_Summand_From_Two_Variables()
        {
            // xy^2
            var tokens = new List<Token>
            {
                Tokens.Variable(Constants.X),
                Tokens.Variable(Constants.Y2)
            };

            var summand = this.builder.Build(tokens).FirstOrDefault();

            Assert.That(summand, Is.Not.Null);
            Assert.That(summand.Value, Is.EqualTo(1d));
            Assert.That(summand.Variables.Count, Is.EqualTo(2));
        }

        [Test]
        public void Test_Building_Of_Multiple_Summands_From_Add_Operation()
        {
            // x + 2y
            var tokens = new List<Token>
            {
                Tokens.Variable(Constants.X),
                Tokens.Operator(Operators.Add),
                Tokens.Scalar(Constants.Two),
                Tokens.Variable(Constants.Y)
            };

            var summands = this.builder.Build(tokens).ToList();

            var first = summands.First();
            var last = summands.Last();
            
            Assert.That(first.Value, Is.EqualTo(1));
            Assert.That(last.Value, Is.EqualTo(2));

            Assert.That(last.Variables.Count, Is.EqualTo(1));
            Assert.That(last.Variables.Count, Is.EqualTo(1));
        }

        [Test]
        public void Test_Building_Of_Multiple_Summands_From_Subtract_Operation()
        {
            // x - y
            var tokens = new List<Token>
            {
                Tokens.Variable(Constants.X),
                Tokens.Operator(Operators.Subtract),
                Tokens.Variable(Constants.Y)
            };

            var summands = this.builder.Build(tokens).ToList();

            var first = summands.First();
            var last = summands.Last();

            Assert.That(first.Value, Is.EqualTo(1));
            Assert.That(last.Value, Is.EqualTo(-1));

            Assert.That(last.Variables.Count, Is.EqualTo(1));
            Assert.That(last.Variables.Count, Is.EqualTo(1));
        }
    }
}