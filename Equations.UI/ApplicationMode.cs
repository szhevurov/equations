﻿using System;
using System.IO;

namespace Equations.UI
{
    internal abstract class ApplicationMode
    {
        protected TextReader Input;

        protected TextWriter Output;

        protected bool IsStarted { get; private set; }

        protected ApplicationMode(TextReader input, TextWriter output)
        {
            if (input == null)
            {
                throw new ArgumentNullException("inputStream");
            }

            if (output == null)
            {
                throw new ArgumentNullException("outputStream");
            }

            this.Input = input;
            this.Output = output;
        }

        public void Start()
        {
            if (this.IsStarted)
            {
                return;
            }

            this.IsStarted = true;

            this.StartInternal();
        }

        protected abstract void StartInternal();
    }
}