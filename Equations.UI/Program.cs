﻿using System;

namespace Equations.UI
{
    class Program
    {
        private const string InteractiveModeKey = "1";

        private const string FileModeKey = "2";

        static void Main(string[] args)
        {
            ApplicationMode mode = null;

            WriteTitle();

            while (true)
            {
                var modeKey = Console.ReadLine();

                if (modeKey != null && modeKey.Equals(InteractiveModeKey))
                {
                    mode = new InteractiveMode(Console.In, Console.Out);
                    break;
                }

                if (modeKey != null && modeKey.Equals(FileModeKey))
                {
                    mode = new FileMode(Console.In, Console.Out);
                    break;
                }

                Console.WriteLine(Resources.UI_Errors.UnknownModeKey);
            }

            mode.Start();

            Console.WriteLine(Resources.UI_Messages.PressAnyKeyToExit);
            Console.ReadKey();
        }

        private static void WriteTitle()
        {
            Console.WriteLine(Resources.UI_Messages.ChooseApplicationMode);
            Console.WriteLine(Resources.UI_Messages.InteractiveModeDescription);
            Console.WriteLine(Resources.UI_Messages.FileModeDescription);
        }
    }
}
