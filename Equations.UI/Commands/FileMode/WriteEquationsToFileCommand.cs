﻿using System;
using System.IO;
using System.Linq;

namespace Equations.UI.Commands.FileMode
{
    internal sealed class WriteEquationsToFileCommand: Command<WriteEquationsToFileParameter, int>
    {
        private const string OutputFileExtension = ".out";

        public WriteEquationsToFileCommand(TextReader input, TextWriter output) : base(input, output)
        {
        }

        public override CommandResult<int> Execute(WriteEquationsToFileParameter parameter)
        {
            var output = Path.ChangeExtension(parameter.OutputFilePath, OutputFileExtension);
            var equations = parameter.Equations;

            var linesWritten = 0;

            if (!equations.Any())
            {
                return new CommandResult<int>(linesWritten);
            }

            try
            {
                if (File.Exists(output))
                {
                    File.Delete(output);
                }

                using (var fs = File.OpenWrite(output))
                {
                    using (var writer = new StreamWriter(fs))
                    {
                        foreach (var equation in equations)
                        {
                            writer.WriteLine(equation);
                            linesWritten++;
                        }
                    }
                }
            }
            catch (PathTooLongException)
            {
                this.Output.WriteLine(Resources.UI_Errors.FilePathIsTooLong);
                return new CommandResult<int>(linesWritten);
            }
            catch (UnauthorizedAccessException)
            {
                this.Output.WriteLine(Resources.UI_Errors.FileAccessIsDenied);
                return new CommandResult<int>(linesWritten); ;
            }
            catch (IOException)
            {
                this.Output.WriteLine(Resources.UI_Errors.UnableToWriteFile);
                return new CommandResult<int>(linesWritten);
            }

            return new CommandResult<int>(linesWritten);
        }
    }
}