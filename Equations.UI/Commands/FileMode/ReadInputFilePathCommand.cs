﻿using System.IO;
using Equations.UI.Validation;

namespace Equations.UI.Commands.FileMode
{
    internal sealed class ReadInputFilePathCommand: Command<CommandParameter, string>
    {
        private readonly IFilePathValidator inputFileValidator = new InputFilePathValidator();

        public ReadInputFilePathCommand(TextReader input, TextWriter output) : base(input, output)
        {
        }

        public override CommandResult<string> Execute(CommandParameter parameter)
        {
            while (true)
            {
                this.Output.WriteLine(Resources.UI_Messages.EnterInputFilePath);

                var inputFilePath = this.Input.ReadLine();

                if (!this.inputFileValidator.Validate(inputFilePath))
                {
                    this.Output.WriteLine(Resources.UI_Errors.InvalidInputFilePath);
                    continue;
                }

                return new CommandResult<string>(inputFilePath);
            }
        }
    }
}