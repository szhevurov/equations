﻿using System;
using System.Collections.Generic;
using System.IO;
using Equations.Symbolics;
using Equations.Symbolics.Tokenize;

namespace Equations.UI.Commands.FileMode
{
    internal sealed class BatchEquationsReductionCommand: Command<GenericCommandParameter<List<string>>, List<string>>
    {
        public BatchEquationsReductionCommand(TextReader input, TextWriter output) : base(input, output)
        {
        }

        public override CommandResult<List<string>> Execute(GenericCommandParameter<List<string>> parameter)
        {
            int errorsCount = 0;
            var equationsInCanonicalForm = new List<string>();

            foreach (var equation in parameter.Data)
            {
                try
                {
                    equationsInCanonicalForm.Add(EquationPrinter.PrintCanonicalForm(equation));
                }
                catch (SyntaxException)
                {
                    errorsCount++;
                }
                catch (ArgumentException)
                {
                    errorsCount++;
                }
            }

            var totalEquationsCount = parameter.Data.Count;
            var successfullyProcessed = totalEquationsCount - errorsCount;

            this.Output.WriteLine(Resources.UI_Messages.BatchEquationReductionResults, totalEquationsCount, successfullyProcessed, errorsCount);

            return new CommandResult<List<string>>(equationsInCanonicalForm);
        }
    }
}