﻿using System.IO;
using Equations.UI.Validation;

namespace Equations.UI.Commands.FileMode
{
    internal sealed class ReadOutputFilePathCommand: Command<CommandParameter, string>
    {
        private readonly IFilePathValidator outputFileValidator = new OutputFilePathValidator();

        public ReadOutputFilePathCommand(TextReader input, TextWriter output) : base(input, output)
        {
        }

        public override CommandResult<string> Execute(CommandParameter parameter)
        {
            while (true)
            {
                this.Output.WriteLine(Resources.UI_Messages.EnterOutputFilePath);

                var outputFilePath = this.Input.ReadLine();

                if (!this.outputFileValidator.Validate(outputFilePath))
                {
                    this.Output.WriteLine(Resources.UI_Errors.InvalidOutputFilePath);
                    continue;
                }

                return new CommandResult<string>(outputFilePath);
            }
        }
    }
}