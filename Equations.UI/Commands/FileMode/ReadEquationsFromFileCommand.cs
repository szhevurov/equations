﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Equations.UI.Commands.FileMode
{
    internal sealed class ReadEquationsFromFileCommand: Command<GenericCommandParameter<string>, List<string>>
    {
        public ReadEquationsFromFileCommand(TextReader input, TextWriter output) : base(input, output)
        {
        }

        public override CommandResult<List<string>> Execute(GenericCommandParameter<string> parameter)
        {
            var emptyResult = new CommandResult<List<string>>(new List<string>());
            var equations = new List<string>();

            try
            {
                using (var fs = File.OpenRead(parameter.Data))
                {
                    using (var reader = new StreamReader(fs, Encoding.UTF8))
                    {
                        while (!reader.EndOfStream)
                        {
                            equations.Add(reader.ReadLine());
                        }
                    }
                }
            }
            catch (PathTooLongException)
            {
                this.Output.WriteLine(Resources.UI_Errors.FilePathIsTooLong);
                return emptyResult;
            }
            catch (UnauthorizedAccessException)
            {
                this.Output.WriteLine(Resources.UI_Errors.FileAccessIsDenied);
                return emptyResult;
            }
            catch (IOException)
            {
                this.Output.WriteLine(Resources.UI_Errors.UnableToReadFile);
                return emptyResult;
            }

            return new CommandResult<List<string>>(equations);
        }
    }
}