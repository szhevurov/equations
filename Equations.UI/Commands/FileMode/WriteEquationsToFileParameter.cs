﻿using System.Collections.Generic;

namespace Equations.UI.Commands.FileMode
{
    internal sealed class WriteEquationsToFileParameter: CommandParameter
    {
        public List<string> Equations { get; set; }

        public string OutputFilePath { get; set; }

        public WriteEquationsToFileParameter(string outputFilePath, List<string> equations)
        {
            this.Equations = equations;
            this.OutputFilePath = outputFilePath;
        }
    }
}