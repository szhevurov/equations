﻿using System.IO;

namespace Equations.UI.Commands
{
    internal abstract class Command<TParam, TResult> : ICommand<TParam, TResult> where TParam : CommandParameter
    {
        protected TextReader Input;
        protected TextWriter Output;

        internal Command(TextReader input, TextWriter output)
        {
            this.Input = input;
            this.Output = output;
        }

        public abstract CommandResult<TResult> Execute(TParam parameter);
    }
}