﻿namespace Equations.UI.Commands
{
    internal class CommandResult<TResult>
    {
        internal TResult Result { get; private set; }

        public CommandResult(TResult result)
        {
            this.Result = result;
        }
    }
}