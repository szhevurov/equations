﻿using System;
using System.IO;
using Equations.Symbolics;
using Equations.Symbolics.Tokenize;

namespace Equations.UI.Commands.InteractiveMode
{
    internal sealed class ReduceEquationCommand: Command<GenericCommandParameter<string>, string>
    {
        public ReduceEquationCommand(TextReader input, TextWriter output) : base(input, output)
        {
        }

        public override CommandResult<string> Execute(GenericCommandParameter<string> parameter)
        {
            var canonicalEquation = string.Empty;
            try
            {
                canonicalEquation = EquationPrinter.PrintCanonicalForm(parameter.Data);
            }
            catch (SyntaxException)
            {
                this.WriteErrorMessage();
            }
            catch (ArgumentException)
            {
                this.WriteErrorMessage();
            }

            return new CommandResult<string>(canonicalEquation);
        }

        private void WriteErrorMessage()
        {
            this.Output.WriteLine(Resources.UI_Errors.UnableToProcessEquation);
        }
    }
}