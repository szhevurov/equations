﻿namespace Equations.UI.Commands
{
    internal interface ICommand<in TParam, TResult> where TParam : CommandParameter
    {
        CommandResult<TResult> Execute(TParam parameter);
    }
}