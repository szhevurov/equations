﻿namespace Equations.UI.Commands
{
    internal class GenericCommandParameter<T>: CommandParameter
    {
        internal virtual T Data { get; private set; }

        public GenericCommandParameter()
        {
        }

        public GenericCommandParameter(T data)
        {
            this.Data = data;
        }
    }
}