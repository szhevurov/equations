﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Equations.UI.Commands;
using Equations.UI.Commands.FileMode;

namespace Equations.UI
{
    internal sealed class FileMode : ApplicationMode
    {
        private readonly ReadInputFilePathCommand readInputFilePathPathCommand;

        private readonly ReadOutputFilePathCommand readOutputFilePathPathCommand;

        private readonly ReadEquationsFromFileCommand readFromFileCommand;

        private readonly BatchEquationsReductionCommand equationReductionCommand;

        private readonly WriteEquationsToFileCommand writeEquationsToFileCommand;

        public FileMode(TextReader input, TextWriter output) : base(input, output)
        {
            this.readInputFilePathPathCommand = new ReadInputFilePathCommand(this.Input, this.Output);
            this.readOutputFilePathPathCommand = new ReadOutputFilePathCommand(this.Input, this.Output);

            this.readFromFileCommand = new ReadEquationsFromFileCommand(this.Input, this.Output);
            this.equationReductionCommand = new BatchEquationsReductionCommand(this.Input, this.Output);

            this.writeEquationsToFileCommand = new WriteEquationsToFileCommand(this.Input, this.Output);
        }

        protected override void StartInternal()
        {
            var inputFilePathResult = this.readInputFilePathPathCommand.Execute(CommandParameter.Empty);
            var outputFilePathResult = this.readOutputFilePathPathCommand.Execute(CommandParameter.Empty);

            var equations = this.readFromFileCommand.Execute(new GenericCommandParameter<string>(inputFilePathResult.Result));
            if (equations.Result.Any())
            {
                var reducedEquations = this.equationReductionCommand.Execute(new GenericCommandParameter<List<string>>(equations.Result));
                this.writeEquationsToFileCommand.Execute(new WriteEquationsToFileParameter(outputFilePathResult.Result, reducedEquations.Result));
            }
            else
            {
                this.Output.WriteLine(Resources.UI_Errors.FileIsEmpty);
            }
        }
    }
}