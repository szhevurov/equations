﻿using System;
using System.IO;
using Equations.UI.Commands;
using Equations.UI.Commands.InteractiveMode;

namespace Equations.UI
{
    internal sealed class InteractiveMode: ApplicationMode
    {
        private readonly ReduceEquationCommand reduceEquationCommand;

        public InteractiveMode(TextReader input, TextWriter output) : base(input, output)
        {
            this.reduceEquationCommand = new ReduceEquationCommand(this.Input, this.Output);
        }

        protected override void StartInternal()
        {
            while (true)
            {
                this.Output.WriteLine(Resources.UI_Messages.EnterEquation);
                var inputLine = this.Input.ReadLine();

                var result = this.reduceEquationCommand.Execute(new GenericCommandParameter<string>(inputLine));

                if (!string.IsNullOrWhiteSpace(result.Result))
                {
                    Console.WriteLine(Resources.UI_Messages.CanonicalForm + result.Result);   
                }
            }
        }
    }
}