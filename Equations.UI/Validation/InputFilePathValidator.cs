﻿using System.IO;

namespace Equations.UI.Validation
{
    internal sealed class InputFilePathValidator : IFilePathValidator
    {
        public bool Validate(string filePath)
        {
            if (string.IsNullOrWhiteSpace(filePath) || !File.Exists(filePath))
            {
                return false;
            }

            return true;
        }
    }
}