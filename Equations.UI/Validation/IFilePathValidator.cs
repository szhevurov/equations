﻿namespace Equations.UI.Validation
{
    internal interface IFilePathValidator
    {
        bool Validate(string filePath);
    }
}