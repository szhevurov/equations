﻿namespace Equations.UI.Validation
{
    internal sealed class OutputFilePathValidator: IFilePathValidator
    {
        public bool Validate(string filePath)
        {
            return !string.IsNullOrWhiteSpace(filePath);
        }
    }
}